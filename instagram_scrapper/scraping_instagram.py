#!/usr/bin/env python
# coding: utf-8

# ## Scraping Instagram menggunakan Instaloader

# Library
from fileinput import filename
import getpass
from instaloader import Instaloader, Profile
import instaloader
from datetime import datetime
from dateutil import parser
from itertools import dropwhile, takewhile
import pandas as pd
import process_code
import numpy as np
import gspread
import json
from oauth2client.service_account import ServiceAccountCredentials
pd.options.mode.chained_assignment = None  # default='warn'

# buka file JSON
file_json = open("dataku.json")

# prsing data JSON
ig = json.loads(file_json.read())

# read csv
dataUser = pd.read_excel('data_akun.xlsx') 
listUser = dataUser['username'].values.tolist()
#print(listUser)

L = Instaloader()
data = []


# ## Function
def Login(USER,PASSWORD):
    # login setup and process
    USER = USER
    PASSWORD = PASSWORD
    L.login(USER,PASSWORD)
    
def get_menu():
    print('Menu')
    print('1. Get data all account')
    print('2. Get data spesific account')
    print('3. Get data in range date')
    
def menu(number):
    textInput = int(number)

    if(textInput == 1):
        print("Ambil Data")
        for akun in listUser:
            df = get_all_data(akun)
            print("Pengambilan data akun " + akun + " berhasil.")
        df.to_csv('DatasetInstagram.csv', index=False)
        return df

    elif(textInput == 2):
        print("Ambil Data")
        df = get_all_data(input("Username : "))
        df.to_csv('DatasetInstagram.csv', index=False)
        print("Pengambilan data akun berhasil.")
        return df
    
    elif(textInput == 3):
        print("Ambil Data")
        date = parser.parse(input("Dari tanggal (YYYY-MM-DD): "))
        for akun in listUser:
            df = get_data(akun, date)
            print("Pengambilan data akun " + akun + " berhasil.")
        return df
        
    else:
        print('Wrong input')
        
    return df

def get_all_data(username):
    # Function get data all account
    df = pd.DataFrame()
    profil = instaloader.Profile.from_username(L.context, username)
    posts = instaloader.Profile.from_username(L.context, username).get_posts()
    for post in posts:
        post_type = ""
        folls = profil.followers
        caption = post.caption
        owner = post.owner_username
        like = post.likes
        com = post.comments
        date = post.date
        url = "https://www.instagram.com/p/" + post.shortcode + "/"
                    
        if (post.typename == 'GraphImage') :
            post_type = "Single"
        elif (post.typename == 'GraphVideo') :
            post_type = "Video"
        elif (post.typename == 'GraphSidecar') :
            post_type = "Carousel"
        else :
            post_type = post.typename
        
        temp = [date, owner, folls, post_type, like, com, caption, url]
        data.append(temp)
        col = ['date', 'user_id', 'followers', 'post_type', 'total_likes', 'total_comments', 'caption', 'url']
        df = pd.DataFrame(data, columns = col)
    return df

def get_data(username, date):
    # Function get data in range date
    df = pd.DataFrame()
    SINCE = date
    UNTIL = SINCE + timedelta(days=7)
    profil = instaloader.Profile.from_username(L.context, username)
    posts = instaloader.Profile.from_username(L.context, username).get_posts()
    for post in takewhile(lambda p: (p.date_utc > SINCE) & (p.date_utc < UNTIL), posts):
        post_type = ""
        folls = profil.followers
        caption = post.caption
        owner = post.owner_username
        like = post.likes
        com = post.comments
        date = post.date
        url = "https://www.instagram.com/p/" + post.shortcode + "/"
                    
        if (post.typename == 'GraphImage') :
            post_type = "Single"
        elif (post.typename == 'GraphVideo') :
            post_type = "Video"
        elif (post.typename == 'GraphSidecar') :
            post_type = "Carousel"
        else :
            post_type = post.typename
        
        temp = [date, owner, folls, post_type, like, com, caption, url]
        data.append(temp)
        col = ['date', 'user_id', 'followers', 'post_type', 'total_likes', 'total_comments', 'caption', 'url']
        df = pd.DataFrame(data, columns = col)
    return df

# Login
print("Login Instagram")
Login(ig['instagram']['user'], ig['instagram']['pass'])
print("Berhasil Login")

# Get menu
get_menu()
menu(int(input("Choose number = ")))

col = ['date', 'user_id', 'followers', 'post_type', 'total_likes', 'total_comments', 'caption', 'url']
df = pd.DataFrame(data, columns = col)

# # Transformation Code
def transform(df):
    temp_df = process_code.duplicate(df)
    temp_df = process_code.handleMistake(df)
    temp_df = process_code.handleNull(df)
    temp_df = process_code.split(df)
    temp_df = process_code.eksperimen1(df)
    return temp_df

result = transform(df)

# # Save Excel
def exportGsheet(sheet, result):
    scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
    creds = ServiceAccountCredentials.from_json_keyfile_name("intern_cred.json", scope)
    client = gspread.authorize(creds)
    spreadsheet = client.open_by_url('https://docs.google.com/spreadsheets/d/1v5-JIBIMwRz47ysgcGmC4Ff3mwFUJY7Kdovul_hPQS8')
    worksheet = spreadsheet.add_worksheet(sheet, len(result.index), 40)
    worksheet.update([result.columns.values.tolist()] + result.values.tolist())

filename = "data scrap date " + date.strftime("%Y-%m-%d")
exportGsheet(filename, result)
print("Data tersimpan.")